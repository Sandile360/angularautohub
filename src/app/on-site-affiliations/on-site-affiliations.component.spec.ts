import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnSiteAffiliationsComponent } from './on-site-affiliations.component';

describe('OnSiteAffiliationsComponent', () => {
  let component: OnSiteAffiliationsComponent;
  let fixture: ComponentFixture<OnSiteAffiliationsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OnSiteAffiliationsComponent]
    });
    fixture = TestBed.createComponent(OnSiteAffiliationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
