import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartsDivisionComponent } from './parts-division.component';

describe('PartsDivisionComponent', () => {
  let component: PartsDivisionComponent;
  let fixture: ComponentFixture<PartsDivisionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PartsDivisionComponent]
    });
    fixture = TestBed.createComponent(PartsDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
