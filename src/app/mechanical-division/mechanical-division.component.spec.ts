import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicalDivisionComponent } from './mechanical-division.component';

describe('MechanicalDivisionComponent', () => {
  let component: MechanicalDivisionComponent;
  let fixture: ComponentFixture<MechanicalDivisionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MechanicalDivisionComponent]
    });
    fixture = TestBed.createComponent(MechanicalDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
