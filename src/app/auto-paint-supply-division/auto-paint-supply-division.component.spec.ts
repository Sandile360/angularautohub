import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoPaintSupplyDivisionComponent } from './auto-paint-supply-division.component';

describe('AutoPaintSupplyDivisionComponent', () => {
  let component: AutoPaintSupplyDivisionComponent;
  let fixture: ComponentFixture<AutoPaintSupplyDivisionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AutoPaintSupplyDivisionComponent]
    });
    fixture = TestBed.createComponent(AutoPaintSupplyDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
