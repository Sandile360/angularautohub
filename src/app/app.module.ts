import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { ServicesComponent } from './services/services.component';
import { ShowOnClickDirective } from './show-on-click.directive';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ContactComponent } from './contact/contact.component';
import { ShopComponent } from './shop/shop.component';
import { AutoPaintSupplyDivisionComponent } from './auto-paint-supply-division/auto-paint-supply-division.component';
import { AutoBodyDivisionComponent } from './auto-body-division/auto-body-division.component';
import { MechanicalDivisionComponent } from './mechanical-division/mechanical-division.component';
import { FourByFourDivisionComponent } from './four-by-four-division/four-by-four-division.component';
import { TyresComponent } from './tyres/tyres.component';
import { TyresAndSuspensionDivisionComponent } from './tyres-and-suspension-division/tyres-and-suspension-division.component';
import { PartsDivisionComponent } from './parts-division/parts-division.component';
import { OnSiteAffiliationsComponent } from './on-site-affiliations/on-site-affiliations.component';
import { AffiliationsComponent } from './affiliations/affiliations.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    ServicesComponent,
    ShowOnClickDirective,
    HomeComponent,
    AboutComponent,
    PortfolioComponent,
    ContactComponent,
    ShopComponent,
    AutoPaintSupplyDivisionComponent,
    AutoBodyDivisionComponent,
    MechanicalDivisionComponent,
    FourByFourDivisionComponent,
    TyresComponent,
    TyresAndSuspensionDivisionComponent,
    PartsDivisionComponent,
    OnSiteAffiliationsComponent,
    AffiliationsComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
