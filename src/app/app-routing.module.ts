import { AffiliationsComponent } from './affiliations/affiliations.component';
import { OnSiteAffiliationsComponent } from './on-site-affiliations/on-site-affiliations.component';
import { PartsDivisionComponent } from './parts-division/parts-division.component';
import { TyresAndSuspensionDivisionComponent } from './tyres-and-suspension-division/tyres-and-suspension-division.component';
import { FourByFourDivisionComponent } from './four-by-four-division/four-by-four-division.component';
import { MechanicalDivisionComponent } from './mechanical-division/mechanical-division.component';
import { AutoPaintSupplyDivisionComponent } from './auto-paint-supply-division/auto-paint-supply-division.component';
import { ShopComponent } from './shop/shop.component';
import { ContactComponent } from './contact/contact.component';
import { HeaderComponent } from './header/header.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ServicesComponent } from './services/services.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { AutoBodyDivisionComponent } from './auto-body-division/auto-body-division.component';



const routes: Routes = [
  { path: '', component: HomeComponent }, // Default route

  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'auto-paint', component: AutoPaintSupplyDivisionComponent },
  { path: 'auto-body', component: AutoBodyDivisionComponent },
  { path: 'mechanical', component: MechanicalDivisionComponent },
  { path: 'four-by-four', component: FourByFourDivisionComponent },
  { path: 'tyres-and-suspension', component: TyresAndSuspensionDivisionComponent },
  { path: 'parts-division', component: PartsDivisionComponent },
  { path: 'on-site-affiliations', component: OnSiteAffiliationsComponent },
  { path: 'affiliations', component: AffiliationsComponent },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
