import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appShowOnClick]'
})
export class ShowOnClickDirective {

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  @HostListener('click')
  onClick() {
    const element = this.elementRef.nativeElement;
    this.renderer.setStyle(element, 'display', 'none');
  }

}
