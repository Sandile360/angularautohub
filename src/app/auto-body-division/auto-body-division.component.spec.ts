import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoBodyDivisionComponent } from './auto-body-division.component';

describe('AutoBodyDivisionComponent', () => {
  let component: AutoBodyDivisionComponent;
  let fixture: ComponentFixture<AutoBodyDivisionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AutoBodyDivisionComponent]
    });
    fixture = TestBed.createComponent(AutoBodyDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
