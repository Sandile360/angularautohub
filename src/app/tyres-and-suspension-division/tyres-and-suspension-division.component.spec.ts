import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TyresAndSuspensionDivisionComponent } from './tyres-and-suspension-division.component';

describe('TyresAndSuspensionDivisionComponent', () => {
  let component: TyresAndSuspensionDivisionComponent;
  let fixture: ComponentFixture<TyresAndSuspensionDivisionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TyresAndSuspensionDivisionComponent]
    });
    fixture = TestBed.createComponent(TyresAndSuspensionDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
