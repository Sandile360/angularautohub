import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FourByFourDivisionComponent } from './four-by-four-division.component';

describe('FourByFourDivisionComponent', () => {
  let component: FourByFourDivisionComponent;
  let fixture: ComponentFixture<FourByFourDivisionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FourByFourDivisionComponent]
    });
    fixture = TestBed.createComponent(FourByFourDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
